<?php
// template Name: Artes
?>

<?php get_header(); ?>
    <h1 id="title">Artes</h1>
        <div class="container">
            <div class="images">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/Arte-4.png">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/Arte-3.png">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/Arte-2.png">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/Arte-1.png">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/Arte.png">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/Arte-5.png">
            </div>
        </div>
<?php get_footer(); ?>        
    