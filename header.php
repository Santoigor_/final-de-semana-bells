<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/reset.css">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/style.css">
    <?php wp_head(); ?>
    <title>Bell's</title>
</head>
<body>
    <header>
        <h1 id="logo">Bell's</h1>
        <nav>
            <ul>
                <li><a href="#livros">Livros</a></li>
                <li><a href="artes">Artes</a></li>
                <li><a href="#sobre">Sobre</a></li>
                <li><a href="#contato">Contato</a></li>
                <li><a href="#login">Login</a></li>
                <li id="btn-doar"><a href="#doar">Doar</a></li>
            </ul>
        </nav>
    </header>